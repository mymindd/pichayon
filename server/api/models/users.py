import mongoengine as me
from werkzeug.security import generate_password_hash, check_password_hash
import datetime

class User(me.Document):
    meta = {'collection': 'users'}

    username = me.StringField(required=True, unique=True)
    password = me.StringField(required=True)
    email = me.StringField()
    first_name = me.StringField(required=True)
    last_name = me.StringField(required=True)
    role = me.ListField(me.StringField(), default=['user'])
    created_data = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
    update_date = me.DateTimeField(required=True,
                                   default=datetime.datetime.utcnow)
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def set_password(self, password):
        self.password=generate_password_hash(password, method='sha256')

    @classmethod
    def authenticated(cls, **kwargs):
        from . import User
        username = kwargs.get('username')
        password = kwargs.get('password')

        if not username or not password:
            return None

        user = cls.objects.get(username = username)

        if not user or not check_password_hash(user.password, password):
            return None
        
        return user

    def to_dict(self):
        return dict(id=str(self.id),
                    username=self.username,
                    first_name=self.first_name,
                    last_name=self.last_name,
                    email=self.email)

