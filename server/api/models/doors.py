import mongoengine as me
import datetime

class Door(me.Document):
    name = me.StringField(required=True, default='-')
    passcode = me.BooleanField(default=False)
    rfid = me.BooleanField(default=False)
    face_recognition = me.BooleanField(default=False)
    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.now)
    updated_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.now)

    meta = {'collection':'doors'}
