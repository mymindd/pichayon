import mongoengine as me
import datetime

from . import User, Door

class Room(me.Document):
    name = me.StringField(required=True, default='-')
    supervisor = me.ReferenceField(User, dbref=True)
    doors = me.ListField(me.ReferenceField(Door, dbref=True))
    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.now)
    updated_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.now)

    meta = {'collection':'rooms'}

