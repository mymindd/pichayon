from flask_mongoengine import MongoEngine
from .users import User
from .doors import Door
from .rooms import Room

db = MongoEngine()

def init_db(app):
    db.init_app(app)
