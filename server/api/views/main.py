from flask import(Blueprint,
                  request,
                  jsonify,
                  current_app)
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from .. import models
from datetime import datetime, timedelta

module = Blueprint('main', __name__, url_prefix='/api')

@module.route('/')
def home():
    models.User.object.get()