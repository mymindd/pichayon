from flask import(Blueprint,
                  request,
                  jsonify,
                  current_app)
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from .. import models
from datetime import datetime, timedelta

module = Blueprint('main', __name__, url_prefix='/api')

@module.route('/login', methods=['POST'])
def login():
    client_data = request.get_json()
    print('xxxx ', client_data)
    user = models.User.authenticated(**client_data)
#     username = client_data.get('username')
#     password = client_data.get('password')

#     if not username or not password:
#         return None

#     user = models.User.objects.get(username = username)
#     if not user or not check_password_hash(user.password, password):
#         return None
    print('user', user)
    if not user:
        return jsonify({ 'message': 'Invalid credentials', 'authenticated': False }), 401
    
    token = jwt.encode({
            'sub': user.username,
            'iat': datetime.utcnow(),
            'exp': datetime.utcnow() + timedelta(minutes=30)
    }, current_app.config['SECRET_KEY'])
    return jsonify({
            'token': token.decode('UTF-8')
    })

@module.route('/register', methods=['POST'])
def register():
    if request.method == 'POST':
        data = request.get_json()
        user = models.User(username=data.get('username'),
                           first_name=data.get('first_name'),
                           last_name=data.get('last_name'),
                           email=data.get('email')
                           )
        user.set_password(data.get('password'))
        user.save()

    return jsonify(user.to_dict())

# @module.route('/logout')
# def logout():
    

# @module.route('/user', methods=['GET', 'POST'])
# def validator():
#     res = {}
#     if request.method == 'POST':
#         print('get data')
#         post_data = request.get_json()
#         print(post_data.get('username'))
#         print(post_data.get('password'))
#     return jsonify(res)