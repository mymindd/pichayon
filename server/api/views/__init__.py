from flask import Flask
from . import users
from . import doors

def register_blueprint(app):
    app.register_blueprint(users.module)
    app.register_blueprint(doors.module)
