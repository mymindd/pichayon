from flask import (Blueprint, 
                    request,
                    redirect,
                    url_for,
                    render_template)
from flask_login import (current_user,
                        login_required)
from .. import models
from .. import mqtt
import datetime

module = Blueprint('doors', __name__, url_prefix='/doors')



@module.route('opendoor/<room_id>:<user>', methods=['GET', 'POST'])
# @login_required
def openlocker(room_id,user):
    # locker = models.Locker.objects.get(id=locker_id)
    # if locker.availability == True:
        # return redirect(url_for('dashboard.index'))
    # else:
    mqtt.opendoor.openDoor(room_id,user)
    return 'open'

