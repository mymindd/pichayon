__version__ = '0.0.1'

from flask import Flask
from flask_cors import CORS

from . import views
from . import models
from . import settings
from . import mqtt


def create_app():
    app = Flask(__name__)
    app.config.from_object(settings)
    app.config.from_envvar('PICHAYON_SETTINGS')

    cors = CORS(app, resource={r'/api/*': {'origins': '*'}})

    models.init_db(app)

    views.register_blueprint(app)

    return app
