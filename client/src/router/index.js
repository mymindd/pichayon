import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

const routerOption = [
  {
    path: '/',
    component: 'Dashboard',
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated) {
        next('/login')
      } else {
        next()
      }
    }
  },
  {
    path: '/rooms',
    component: 'Rooms',
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated) {
        next('/login')
      } else {
        next()
      }
    }
  },
  {
    path: '/room',
    component: 'Room',
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated) {
        next('/login')
      } else {
        next()
      }
    }
  },
  {
    path: '/login',
    component: 'Login'
  },
  {
    path: '/register',
    component: 'Register'
  }
]

const routes = routerOption.map(route => {
  return {
    ...route,
    component: () => import(`@/components/${route.component}.vue`)
  }
})

Vue.use(Router)

export default new Router({
  routes,
  mode: 'history'
})
