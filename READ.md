Client:
  npm install install
  npm run dev

Server:
  pyvenv env
  source env/bin/activate
  pip install -r requirements.txt
  ./scripts/run.sh